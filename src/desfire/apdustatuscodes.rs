#[allow(dead_code)]
#[repr(u16)]
pub enum APDUStatusCodes {
    /// Successful operation
    OperationOk = 0x9000,

    /// No changes done to backup files, CommitTransaction / AbortTransaction not necessary
    NoChanges = 0x900C,

    /// Insufficient NV-Memory to complete command
    OutOfEepromError = 0x900E,

    /// Command code not supported
    IllegalCommandCode = 0x901C,

    /// CRC or MAC does not match data Padding bytes not valid
    IntegrityError = 0x901E,

    /// Invalid key number specified
    NoSuchKey = 0x9040,

    /// Invalid key number specified
    LengthError = 0x907E,

    /// Current configuration / status does not allow the requested command
    PermissionDenied = 0x909D,

    /// Value of the parameter(s) invalid
    ParameterError = 0x909E,

    /// Requested AID not present on PICC
    ApplicationNotFound = 0x90A0,

    /// Unrecoverable error within application, application will be disabled
    ApplIntegrityError = 0x90A1,

    /// Current authentication status does not allow the requested command
    AuthenticationError = 0x90AE,

    /// Additional data frame is expected to be sent
    AdditionalFrame = 0x90AF,

    /// Attempt to read/write data from/to beyond the file\'s/record\'s limits. Attempt to exceed the limits of a value file.
    BoundaryError = 0x90BE,

    /// Unrecoverable error within PICC, PICC will be disabled
    PiccIntegrityError = 0x90C1,

    /// Previous Command was not fully completed Not all Frames were requested or provided by the PCD
    CommandAborted = 0x90CA,

    /// PICC was disabled by an unrecoverable error
    PiccDisabledError = 0x90CD,

    /// Number of Applications limited to 28, no additional CreateApplication possible
    CountError = 0x90CE,

    /// Creation of file/application failed because file/application with same number already exists
    DuplicateError = 0x90DE,

    /// Could not complete NV-write operation due to loss of power, internal backup/rollback mechanism activated
    EepromError = 0x90EE,

    /// Specified file number does not exist
    FileNotFound = 0x90F0,

    /// Unrecoverable error within file, file will be disabled
    FileIntegrityError = 0x90F1,
}
