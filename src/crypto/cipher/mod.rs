pub mod aes;
pub mod tdes;
pub mod tdes_2k;
pub mod tdes_3k;

use crate::error::Result;

//TODO: impl this for all the ciphers
pub trait Cipher {
    const BLOCK_SIZE: usize;
    const KEY_SIZE: usize;

    fn encrypt(data: &[u8], key: &[u8], iv: &[u8]) -> Result<Vec<u8>>;
    fn decrypt(data: &[u8], key: &[u8], iv: &[u8]) -> Result<Vec<u8>>;
}
