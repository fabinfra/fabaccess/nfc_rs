//FIXME: Which TDES Mode should be used by this?
use crate::crypto::cipher::Cipher;
use crate::error::{Error, Result};
use block_modes::block_padding::NoPadding;
use block_modes::{BlockMode, Cbc};
use des::TdesEde3;

type TDesEde3Cbc = Cbc<TdesEde3, NoPadding>;

pub struct Tdes3k {}

impl Cipher for Tdes3k {
    const BLOCK_SIZE: usize = 8;
    const KEY_SIZE: usize = 24;

    fn encrypt(data: &[u8], key: &[u8], iv: &[u8]) -> Result<Vec<u8>> {
        let cipher = TDesEde3Cbc::new_from_slices(key, iv)?;

        Ok(cipher.encrypt_vec(data))
    }

    fn decrypt(data: &[u8], key: &[u8], iv: &[u8]) -> Result<Vec<u8>> {
        let cipher = TDesEde3Cbc::new_from_slices(key, iv)?;

        let result = cipher.decrypt_vec(data);
        match result {
            Ok(data) => Ok(data),
            Err(err) => Err(Error::BlockModeError(err)),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::crypto::cipher::Cipher;
    use hex_literal::hex;

    #[test]
    #[ignore]
    fn encrypt() {
        let data = hex!("");
        let key = hex!("00000000000000000000000000000000");
        let iv = hex!("00000000000000000000000000000000");

        let data_enc = crate::crypto::cipher::tdes_3k::Tdes3k::encrypt(&data, &key, &iv).unwrap();

        let data_enc_expected = hex!("");

        assert_eq!(data_enc, data_enc_expected);
    }
}
