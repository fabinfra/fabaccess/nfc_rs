use super::Cipher;
use crate::error::{Error, Result};
use aes::Aes128;
use block_modes::block_padding::NoPadding;
use block_modes::{BlockMode, Cbc};

type Aes128Cbc = Cbc<Aes128, NoPadding>;

pub struct AES {}

impl Cipher for AES {
    const BLOCK_SIZE: usize = 16;
    const KEY_SIZE: usize = 16;

    fn encrypt(data: &[u8], key: &[u8], iv: &[u8]) -> Result<Vec<u8>> {
        let cipher = Aes128Cbc::new_from_slices(key, iv)?;

        Ok(cipher.encrypt_vec(data))
    }

    fn decrypt(data: &[u8], key: &[u8], iv: &[u8]) -> Result<Vec<u8>> {
        let cipher = Aes128Cbc::new_from_slices(key, iv)?;

        let result = cipher.decrypt_vec(data);
        match result {
            Ok(data) => Ok(data),
            Err(err) => Err(Error::BlockModeError(err)),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::crypto::cipher::Cipher;
    use hex_literal::hex;

    #[test]
    fn encrypt() {
        let data = hex!("8db1f942f2d7cc82f6fa1486a30f8c12104a3b07e8eb77a7ac00000000000000");
        let key = hex!("e7aff3361c3e85347993c3219a87d24b");
        let iv = hex!("00000000000000000000000000000000");

        let data_enc = crate::crypto::cipher::aes::AES::encrypt(&data, &key, &iv).unwrap();

        let data_enc_expected =
            hex!("3c79d74a4969ba7123e5d8f6df24493112d221fd131a4617d0eda5d92ccc1b46");

        assert_eq!(data_enc, data_enc_expected);
    }
}
