use std::slice::from_ref;

const POLY: u16 = 0x8408;
const INIT_VALUE: u16 = 0x6363;

pub fn calculate_with_initial(data: &[u8], mut crc16: u16) -> u16 {
    for b in data {
        crc16 ^= *b as u16;
        for _ in 0..8 {
            let b_bit = (crc16 & 0x01) > 0;
            crc16 >>= 1;
            if b_bit {
                crc16 ^= POLY;
            }
        }
    }
    crc16
}

pub fn calculate(data: &[u8]) -> Box<[u8]> {
    let mut crc16 = INIT_VALUE;

    for d in data {
        crc16 = calculate_with_initial(from_ref(d), crc16);
    }

    Box::new(crc16.to_ne_bytes())
}

#[cfg(test)]
mod tests {
    use hex_literal::hex;

    #[test]
    #[ignore]
    fn calculate() {
        let data = hex!("");
        let crc_expected = hex!("");

        let crc = crate::crypto::crc::crc16::calculate(&data);

        assert_eq!(*crc, crc_expected);
    }
}
