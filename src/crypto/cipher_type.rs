#[derive(Debug, PartialEq, Clone)]
pub enum CipherType {
    /// DES / Triple DES
    TDES,

    /// Triple DES with 2 DES Keys
    TDES2K,

    /// Triple DES with 3 DES Keys
    TDES3K,

    /// AES
    AES,
}
